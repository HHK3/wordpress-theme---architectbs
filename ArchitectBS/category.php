<?php get_header(); ?>

<section id="primary" class="site-content">
    <div id="content" role="main">


        <header class="archive-header">
            <h1 class="archive-title"><?php printf(__('Categorie: %s', 'learningWordPress'), '<span>' . single_cat_title('', false) . '</span>'); ?></h1>

            <?php if (category_description()) : // Show an optional category description ?>
                <div class="archive-meta"><?php echo category_description(); ?></div>
            <?php endif; ?>
        </header><!-- .archive-header -->

        <?php
        $args_nationaal = array(
            'post_type' => 'work1',
            'category_name' => 'nationaal',
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $args_internationaal = array(
            'post_type' => 'work1',
            'category_name' => 'internationaal',
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $args_prototype = array(
            'post_type' => 'work1',
            'category_name' => 'prototype',
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        $nationaal_query = new WP_Query($args_nationaal);
        $internationaal_query = new WP_Query($args_internationaal);
        $prototype_query = new WP_Query($args_prototype);


        if (strpos($_SERVER['REQUEST_URI'], 'internationaal') !== false) {
            echo do_shortcode("[post_grid id='94']");
        } elseif (strpos($_SERVER['REQUEST_URI'], 'nationaal') !== false) {
            echo do_shortcode("[post_grid id='92']");
        } elseif (strpos($_SERVER['REQUEST_URI'], 'prototype') !== false) {
            echo do_shortcode("[post_grid id='95']");
        }

        ?>


    </div><!-- #content -->
</section><!-- #primary -->

<?php get_footer(); ?>

