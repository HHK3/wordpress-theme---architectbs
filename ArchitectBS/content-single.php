<div class="blog-post">
    <h2 class="blog-post-title"><?php the_title(); ?></h2>
    <?php if (strpos($_SERVER['REQUEST_URI'], 'work1') == false) { ?>
        <p class="blog-post-meta"><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p><?php
    } ?>

    <?php if (strpos($_SERVER['REQUEST_URI'], 'work1') !== false) {
        the_category();
    } ?>

    <?php if (has_post_thumbnail()) {
        the_post_thumbnail();
    } ?>

    <?php the_content(); ?>
</div><!-- /.blog-post -->

<?php if (strpos($_SERVER['REQUEST_URI'], 'work1') !== false) { ?>
    <hr>
    <h1>Verwante voorbeelden van Work</h1>
    <?php echo do_shortcode("[post_grid id='83']"); ?>
<?php }