<?php

get_header();
?>
    <img src="<?php bloginfo('template_directory'); ?>/images/home.png" alt="Home Pic" class="bigpic">
    <hr>
    <h1 class="blog-title">Work</h1>
    <div class="row">

        <div class="col-sm-12">

            <?php

            get_template_part('content-single-home', get_post_format());

            ?>

        </div> <!-- /.col -->

    </div> <!-- /.row -->
    <hr>
    <h1 class="blog-title">Post</h1>
    <div class="row">

        <div class="col-sm-12">

            <?php

            get_template_part('content-news', get_post_format());

            ?>

        </div> <!-- /.col -->

    </div> <!-- /.row -->
<?php
get_footer();