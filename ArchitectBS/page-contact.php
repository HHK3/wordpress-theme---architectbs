<?php get_header(); ?>
    <h1 class="heading1">Contact</h1>

    <div class="row">

        <div class="col-sm-12">

            <?php echo do_shortcode("[wpgmza id='1']"); ?>
            <div id="contact">
                <h2>Contactgegevens</h2>
                <h4>Contactweg 36</h4>
                <h4>1014 AN Amsterdam</h4>
            </div>

            <div id="contactform">
                <?php echo do_shortcode(" [wpforms id='76' title='true']"); ?>
            </div>


        </div> <!-- /.col -->

    </div> <!-- /.row -->

<?php get_footer(); ?>