<?php get_header(); ?>
    <h1 class="heading1">News</h1>
    <div id="date">
        <h2><?php _e('Sorteren op maand'); ?></h2>
        <select name="archive-dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
            <option value="">
                <?php echo esc_attr(__('Selecteer maand')); ?>
            </option>
            <?php wp_get_archives(array('type' => 'monthly', 'format' => 'option', 'show_post_count' => 1)); ?>
        </select>
    </div>
    <div class="row">

        <div class="col-sm-12">

            <?php

            get_template_part('content-news', get_post_format());

            ?>

        </div> <!-- /.col -->

    </div> <!-- /.row -->

<?php get_footer(); ?>