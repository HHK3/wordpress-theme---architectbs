<?php get_header(); ?>
<h1 class="heading1">Work</h1>

<div class="content-search">
    <li id="categories">
        <h2><?php _e('Filter Categorie'); ?></h2>
        <form id="category-select" class="category-select" action="<?php echo esc_url(home_url('/')); ?>" method="get">
            <?php
            $args = array(
                'show_option_none' => __('Selecteer Categorie'),
                'exclude' => 6,
                'show_count' => 1,
                'orderby' => 'name',
                'echo' => 0,
            );
            ?>

            <?php $select = wp_dropdown_categories($args); ?>
            <?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
            <?php $select = preg_replace('#<select([^>]*)>#', $replace, $select); ?>

            <?php echo $select; ?>

            <noscript>
                <input type="submit" value="View"/>
            </noscript>
        </form>
    </li>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php echo do_shortcode("[post_grid id='58']"); ?>

    </div> <!-- /.col -->
</div> <!-- /.row -->

<?php get_footer(); ?>
