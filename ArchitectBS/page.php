<?php get_header(); ?>

    <div class="row">

        <div class="col-sm-12">

            <?php

            get_template_part('content', get_post_format());

            ?>

        </div> <!-- /.col -->

    </div> <!-- /.row -->

<?php get_footer(); ?>