<?php

get_header(); ?>

    <!-- site-content -->
    <div class="site-content clearfix">

        <!-- main-column -->
        <div class="main-column">

            <?php

            if (have_posts()) : ?>

                <h2>Zoekresultaten voor: <?php the_search_query(); ?></h2>

                <?php
                while (have_posts()) : the_post();

                    get_template_part('content', get_post_format());

                endwhile;

            else :?>
                <h2>Er zijn helaas geen zoekresultaten gevonden voor: <?php the_search_query(); ?>

            <?php endif;

            ?>

        </div><!-- /main-column -->


    </div><!-- /site-content -->

<?php get_footer();

?>