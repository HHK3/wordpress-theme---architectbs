WP Theme: ArchitectBS
=====================
This WP theme was made for the subject CMS (Content Management System) for my website: AC Architects.  
I've used several plugins for this theme.

Plugins
-------
* Aksimet
* Post-Grid
* WPForms Lite
* WP Google Maps

Website
-------
http://25061.hosts1.ma-cloud.nl/wordpress/