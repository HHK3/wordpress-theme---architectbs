var donkerAan = false;

jQuery(document).ready(function ($) {
    $(".dark").on("click", switchDonkerModus);
});

function switchDonkerModus() {
    donkerAan = !donkerAan;
    if (donkerAan) {
        $("body").addClass("darkmode");
        $("footer").addClass("darkmode");
        $(".dark").text("Turn lights on");

    } else {
        $("body").removeClass("darkmode");
        $("footer").removeClass("darkmode");
        $(".dark").text("Turn lights off");
    }
}

