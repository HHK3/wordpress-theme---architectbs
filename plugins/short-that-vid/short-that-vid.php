<?php
/*
Plugin Name:  Short That Vid
Description:  Embed YT/Vimeo/Dailymotion/Dumpert vids using  Shortcodes
Version:      V1.0
Author:       Joël Lakhai
Author URI:   http://25061.hosts1.ma-cloud.nl/portfolio/dist/
License:      GPL2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  test-portfolio
Domain Path:  /languages
*/

function videoShortCode($atts, $content = null, $website){

    extract( shortcode_atts( array(
        'width' => '500',
        'id' => '',
        'height' => '300',
    ), $atts ) );

    switch( $website ) {
        case "youtube":
            $source = 'http://www.youtube-nocookie.com/embed/';
            break;
        case "vimeo":
            $source = 'http://player.vimeo.com/video/';
            break;
        case "dailymotion":
            $source = 'http://www.dailymotion.com/embed/video/';
            break;
        case "dumpert":
            $source = 'http://www.dumpert.nl/embed/';
            break;
    }

    if ( $id != '') {

        wp_enqueue_style('videocss', plugin_dir_url( __FILE__ ) . '/css/video.css', array());
        wp_register_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js', null, null, true );
        wp_enqueue_script('jQuery');
        wp_enqueue_script('lights', plugin_dir_url( __FILE__ ) . '/js/donkermode.js', array());

        if ( $website == "youtube" ) {
            return '<div class="shortcodeVid">
                        <img src="' . plugin_dir_url( __FILE__ ) . '/images/yt.png' . '" alt="Youtube" class="banner"/> 
                        <iframe src="' . $source . $id . '" class="iframevideo" height="' . $height . '" width="' . $width . '"></iframe>
                        <br>
                        <button class="dark">Turn lights off</button>
                    </div>';
        }

        elseif ( $website == "vimeo" ) {
            return '<div class="shortcodeVid">
                        <img src="' . plugin_dir_url( __FILE__ ) . '/images/vimeo.png' . '" alt="Vimeo" class="banner"/> 
                        <iframe src="' . $source . $id . '" class="iframevideo" height="' . $height . '" width="' . $width . '"></iframe>
                        <br>
                        <button class="dark">Turn lights off</button>
                    </div>';
        }

        elseif ( $website == "dailymotion" ) {
            return '<div class="shortcodeVid">
                        <img src="' . plugin_dir_url( __FILE__ ) . '/images/daily.png' . '" alt="Dailymotion" class="banner"/> 
                        <iframe src="' . $source . $id . '" class="iframevideo" height="' . $height . '" width="' . $width . '"></iframe>
                        <br>
                        <button class="dark">Turn lights off</button>
                    </div>';
        }

        elseif ( $website == "dumpert" ) {
            return '<div class="shortcodeVid">
                        <img src="' . plugin_dir_url( __FILE__ ) . '/images/dumpert.png' . '" alt="Dumpert" class="banner"/> 
                        <iframe src="' . $source . $id . '" class="iframevideo" height="' . $height . '" width="' . $width . '"></iframe>
                        <br>
                        <button class="dark">Turn lights off</button>
                    </div>';
        }

    }
    return '';

}

add_shortcode( 'youtube', 'videoShortCode' );
add_shortcode( 'vimeo', 'videoShortCode' );
add_shortcode( 'dailymotion', 'videoShortCode' );
add_shortcode( 'dumpert', 'videoShortCode' );
add_action('admin_menu', 'medInstructions_menu');

function medInstructions_menu(){
    add_menu_page( 'Short That Vid - Instructions', 'Short That Vid - Instructions', 'manage_options', 'short-that-vid-instructions', 'showInstructions' );
}

function showInstructions(){
    wp_enqueue_style('videocss', plugin_dir_url( __FILE__ ) . '/css/video.css', array());

    echo "<h1>Instructions</h1>

          <img src='" . plugin_dir_url( __FILE__ ) . "/images/yt.png" . "' alt='Youtube' class='image_admin'/>
          <h3>1. Copy the code after \"v=\"</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/ytlinks.png" . "' alt='Youtube Link' class='image_admin'/>
          <h3>2. Use shortcode [youtube id=\"\"] in the editor and put the copied code between the quotation marks</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/ytshortcode.png" . "' alt='Youtube Shortcode' class='image_admin'/>
          <h3>3. Publish/Update your post and it will have the embedded Youtube Video</h3>
          <br>
          
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/vimeo.png" . "' alt='Vimeo' class='image_admin'/>
          <h3>1. Copy the numbers after \"vimeo.com/\"</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/vimeolinks.png" . "' alt='Vimeo Link' class='image_admin'/>
          <h3>2. Use shortcode [vimeo id=\"\"] in the editor and put the copied numbers between the quotation marks</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/vimeoshortcode.png" . "' alt='Vimeo Shortcode' class='image_admin'/>
          <h3>3. Publish/Update your post and it will have the embedded Vimeo Video</h3>
          <br>
          
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/daily.png" . "' alt='Dailymotion' class='image_admin'/>
          <h3>1. Copy the code after \"/video/\"</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/dailylinks.png" . "' alt='Dailymotion Link' class='image_admin'/>
          <h3>2. Use shortcode [dailymotion id=\"\"] in the editor and put the copied code between the quotation marks</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/dailyshortcode.png" . "' alt='Dailymotion Shortcode' class='image_admin'/>
          <h3>3. Publish/Update your post and it will have the embedded Dailymotion Video</h3>
          <br>
          
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/dumpert.png" . "' alt='Dumpert' class='image_admin'/>
          <h3>1. Copy the string after \"vimeo.com/\" and before \".html\"</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/dumpertlinks.png" . "' alt='Dumpert Link' class='image_admin'/>
          <h3>2. Use shortcode [dumpert id=\"\"] in the editor and put the copied string between the quotation marks</h3>
          <img src='" . plugin_dir_url( __FILE__ ) . "/images/dumpertshortcode.png" . "' alt='Dumpert Shortcode' class='image_admin'/>
          <h3>3. Publish/Update your post and it will have the embedded Dumpert Video</h3>
          <br>";

}