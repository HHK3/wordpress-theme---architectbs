<?php
/**
Plugin Name:  Shortcode_Button
Description:  WordPress Plugin for site: AC Architects
Version:      V0.1
Author:       Joël Lakhai
Author URI:   http://25061.hosts1.ma-cloud.nl/portfolio/dist/
License:      GPL2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  test-portfolio
Domain Path:  /languages
 */

function make_my_button(){
    return "<a href='http://www.google.nl/'' class='button'>Hallo</a>";
}
add_shortcode('my_button', 'make_my_button');